//
//  ViewController.swift
//  custom_switch
//
//  Created by Md Khaled Hasan Manna on 26/11/20.
//

import UIKit

class ViewController: UIViewController,SwiftySwitchDelegate {

    @IBOutlet weak var swiftySwitch: SwiftySwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swiftySwitch.delegate = self
        
    }
    func valueChanged(sender: SwiftySwitch) {
        if swiftySwitch.isOn{
            self.view.backgroundColor = .cyan
        }else{
            self.view.backgroundColor = .white
        }
    }
    



}

